#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"

mkdir k8s-core

cat << EOF > k8s-core.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: k8s-core-apps
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: k8s-core
  destination:
    server: https://kubernetes.default.svc
    namespace: argocd
  syncPolicy:
    automated:
      prune: true
      selfHeal: true  
EOF

mv k8s-core.yaml apps
git add apps
git commit -m "add argoCD application to contain core k8s applications"
git push origin master

cat << EOF > sealed-secrets.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: sealed-secrets-controller
  namespace: argocd
spec:
  project: default
  destination:
    namespace: kube-system
    server: https://kubernetes.default.svc
  source:
    chart: sealed-secrets
    repoURL: https://bitnami-labs.github.io/sealed-secrets
    targetRevision: 1.15.0-r3
  syncPolicy:
    automated:
      prune: true
      selfHeal: true  
EOF

mv sealed-secrets.yaml k8s-core
git add k8s-core
git commit -m "install the sealed secrets controller"
git push origin master

argocd app sync root
