#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"
failcheck="${GITLAB_PAT}failcheck"

for deploy in "dex-server" "redis" "repo-server" "server"; \
  do kubectl -n argocd rollout status deploy/argocd-${deploy}; \
done

port_forwarding_cmd="kubectl -n argocd port-forward svc/argocd-server 8080:443"
$port_forwarding_cmd > /dev/null 2>&1 &

argo_pass=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d)
argocd login localhost:8080 --username admin --password $argo_pass

# TODO: make sure this is removed later
argocd repo add $GITLAB_REPO_URL --username any-non-empty-string --password $GITLAB_PAT

mkdir apps

cat << EOF > root.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: root
  namespace: argocd
spec:
  project: default
  source:
    repoURL: $GITLAB_REPO_URL
    targetRevision: HEAD
    path: apps
  destination:
    server: https://kubernetes.default.svc
    namespace: argocd
  syncPolicy:
    automated:
      prune: true
      selfHeal: true  
EOF

mv root.yaml apps

git add apps
git commit -m "add argoCD root application"
git push origin master

kubectl apply -f apps
argocd app sync root

echo "You should now be able to navigate to localhost:8080 and log into argocd as admin. The password can be retrieved by running:"
echo ""
echo 'kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d'
echo ""
echo "ArgoCD will be the system of record for the deployment state - so keep and eye on what's happening in the panel as these scripts are run."
echo ""
echo "If something goes wrong with port forwarding, you can open a new terminal window and use the following port forwarding command in this script to re-establish the connection."
echo ""
echo "$port_forwarding_cmd"
