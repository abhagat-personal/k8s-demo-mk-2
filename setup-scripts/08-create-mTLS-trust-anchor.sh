#!/bin/bash

set -euo pipefail
export $(cat .env | xargs)

failcheck="${GITLAB_REPO_URL}failcheck"

kubectl create namespace linkerd

cat << EOF > linkerd2-cni.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: linkerd2-cni
  namespace: argocd
spec:
  project: default
  source:
    chart: linkerd2-cni
    repoURL: https://helm.linkerd.io/stable
    targetRevision: 2.10.1
  destination:
    namespace: linkerd-cni
    server: https://kubernetes.default.svc
  syncPolicy:
    automated:
      prune: true
      selfHeal: true  
EOF

mv linkerd2-cni.yaml k8s-core

step certificate create root.linkerd.cluster.local mtls-trust.crt mtls-trust.key \
  --profile root-ca \
  --no-password \
  --not-after 43800h \
  --insecure

step certificate inspect mtls-trust.crt

kubectl -n linkerd create secret tls linkerd-trust-anchor \
  --cert mtls-trust.crt \
  --key mtls-trust.key \
  --dry-run=client -oyaml | \
kubeseal -oyaml - | \
kubectl patch -f - \
  -p '{"spec": {"template": {"type":"kubernetes.io/tls", "metadata": {"labels": {"linkerd.io/control-plane-component":"identity", "linkerd.io/control-plane-ns":"linkerd"}, "annotations": {"linkerd.io/created-by":"linkerd/cli stable-2.8.1", "linkerd.io/identity-issuer-expiry":"2021-07-19T20:51:01Z"}}}}}' \
  --dry-run=client \
  --type=merge \
  --local -oyaml > trust-anchor.yaml

cat << EOF > linkerd-bootstrap.yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: linkerd-bootstrap
  namespace: argocd
spec:
  destination:
    namespace: linkerd
    server: https://kubernetes.default.svc
  project: default
  source:
    path: k8s-core/linkerd
    repoURL: $GITLAB_REPO_URL
  syncPolicy:
    automated: {}
EOF

cat << EOF > namespace.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: linkerd
EOF

cat << EOF > trust-issuer.yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: linkerd-trust-anchor
  namespace: linkerd
spec:
  ca:
    secretName: linkerd-trust-anchor
---
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: linkerd-identity-issuer
  namespace: linkerd
spec:
  secretName: linkerd-identity-issuer
  duration: 730h0m0s
  renewBefore: 1h0m0s
  issuerRef:
    name: linkerd-trust-anchor
    kind: Issuer
  dnsNames:
    - identity.linkerd.cluster.local
  isCA: true
  privateKey:
    algorithm: ECDSA
  usages:
  - cert sign
  - crl sign
  - server auth
  - client auth
EOF

mv linkerd-bootstrap.yaml k8s-core
mkdir k8s-core/linkerd
mv namespace.yaml k8s-core/linkerd
mv trust-anchor.yaml k8s-core/linkerd
mv trust-issuer.yaml k8s-core/linkerd

git add k8s-core
git commit -m "add linkerd bootstrap with trust anchors"
git push origin master

echo "You'll have generated mtls-trust.crt and mtls-trust.key. Save these in your secrets manager as keys."
echo "They will expire, but it's nice to have them as a backup for now."

argocd app sync k8s-core-apps
